import {Component} from '@angular/core';
import {RxStompService} from '../rx-stomp.service';
import {Message} from '@stomp/stompjs';


@Component({
  selector: 'app-greetings',
  templateUrl: './greetings.component.html',
  styleUrls: ['./greetings.component.scss']
})
export class GreetingsComponent {
  name: string = '';
  receivedMessages: string[] = [];

  constructor(private rxStompService: RxStompService) {
    this.rxStompService.watch('/topic/greetings').subscribe((message: Message) => {
      const parsed = JSON.parse(message.body);
      this.receivedMessages.push(parsed.content);
    });
  }

  onSendMessage() {
    const message = JSON.stringify({'name': this.name});
    this.rxStompService.publish({destination: '/app/hello', body: message});
  }
}
