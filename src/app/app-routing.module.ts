import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {GreetingsComponent} from './greetings/greetings.component';

const routes: Routes = [
  { path: 'greetings', component: GreetingsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
